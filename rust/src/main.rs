use std::collections::LinkedList;

// Build and run: cargo run
fn main() {
    let mut container = Vec::new();
    // let mut container = LinkedList::new(); // Try this as well
    container.push("A");

    let mut it1 = container.iter();
    println!("{}", it1.next().unwrap()); // unwrap() retrieves the value: A

    container.push("B");

    println!("{}", it1.next().unwrap()); // Error here, use the following lines instead to compile:
                                         // let mut it2 = container.iter(); // The it1 cannot be used anymore since it might have been invalidated by container.push_back("C");
                                         // it2.next(); // Move passed "A"
                                         // println!("{}", it2.next().unwrap());
}
