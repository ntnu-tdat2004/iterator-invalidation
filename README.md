# Examples demonstrating use of invalid iterators
A container iterator is often invalidated after modifying the container.

## Java comments
* Throws exceptions when reading values of invalid iterators
* Replacing `ArrayList` with `LinkedList` should work, but does not

## C++ comments
* No exceptions when reading values of invalid iterators
    * address sanitizer can be used to identify use of invalid iterators at runtime
    * upcoming static analysis gives compilation warning: https://godbolt.org/z/A2qH97
* Replacing `vector` with `list` works as expected

## Rust comments
* Throws exceptions when reading values of out of range iterators
* Static (borrow) checker prevents you from reusing iterator `it1` after modifying the `container`
    * prevents iterator invalidation at the cost of additional source complexity
* Note: methods `push` must be renamed to `push_back` if `LinkedList` is used instead of `Vec`
