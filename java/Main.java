import java.lang.*;
import java.util.*;

// Compile and run: javac Main.java && java Main
public class Main {
  public static void main(String[] args) {
    List<String> container = new ArrayList<String>();
    // List<String> container = new LinkedList<String>(); // Try this as well
    container.add("A");

    Iterator<String> it = container.iterator();
    System.out.println(it.next()); // Output: A

    container.add("B");
    
    System.out.println(it.next()); // Output: B?
  }
}
