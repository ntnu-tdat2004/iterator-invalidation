#include <iostream>
#include <list>
#include <string>
#include <vector>

using namespace std;

// Compile and run: g++ -fsanitize=address -std=c++11 main.cpp && ./a.out
int main() {
  vector<string> container;
  // list<string> container; // Try this as well
  container.emplace_back("A");

  auto it = container.begin();
  cout << *it << endl; // Dereference operator* retrieves the value: A

  container.emplace_back("B");

  it++;
  cout << *it << endl; // Output: B?
}
